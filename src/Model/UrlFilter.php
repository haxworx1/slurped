<?php

namespace App\Model;

use App\Entity\RobotSettings;

class UrlFilter
{
    public function __construct(
        private string $scheme,
        private string $host,
        private array $urls,
    )
    {

    }

    public function filter(): array
    {
        $filtered = [];

        foreach ($this->urls as $url) {
            $parsedUrl = parse_url($url);
            if ((strtoupper($parsedUrl['scheme']) === strtoupper($this->scheme)) &&
                (strtoupper($parsedUrl['host']) === strtoupper($this->host))) {
                $filtered[] = $url;
            }
        }

        return $filtered;
    }
}