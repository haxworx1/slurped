<?php

declare(strict_types=1);

namespace App\Model;

class UrlResponse
{
    public function __construct(
        private string $url,
        private int $statusCode,
        private array $headers,
        private string $content,
    ) {
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
