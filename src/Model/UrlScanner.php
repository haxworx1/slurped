<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\RobotSettings;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UrlScanner
{
    public function __construct(
        private HttpClientInterface $client,
        private string $url,
    ) {
    }

    public function download(): UrlResponse
    {
        $response = $this->client->request('GET', $this->url);

        return new UrlResponse(
            $this->url,
            $response->getStatusCode(),
            $response->getHeaders(),
            $response->getContent(),
        );
    }
}
