<?php

declare(strict_types=1);

namespace App\Model;

use Symfony\Component\DomCrawler\Crawler;

class UrlExtractor
{
    private Crawler $crawler;

    public function __construct(UrlResponse $urlResponse)
    {
        $this->crawler = new Crawler($urlResponse->getContent(), $urlResponse->getUrl());
    }

    public function extract(string $tag): array
    {
        return $this->crawler->filter($tag)->each(function (Crawler $node, $i) {
            return $node->link()->getUri();
        });
    }
}
