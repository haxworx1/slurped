<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\RobotData;
use App\Model\UrlExtractor;
use App\Model\UrlFilter;
use App\Model\UrlResponse;
use App\Model\UrlScanner;
use App\Repository\RobotDataRepository;
use App\Repository\RobotSettingsRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Uid\Ulid;

#[AsCommand(
    name: 'app:robot',
    description: 'Crawls a remote web site.',
    hidden: false,
    aliases: ['app:robot']
)]
class RobotCommand extends Command
{
    private array $visited = [];
    private Ulid $id;
    private string $url;
    private string $scheme;
    private string $host;

    public function __construct(
        private RobotSettingsRepository $repository,
        private RobotDataRepository $dataRepository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->AddArgument('id', InputArgument::REQUIRED, 'id of remote web site.')
        ;
    }

    protected function store(UrlResponse $response)
    {
        $robot = new RobotData();
        $robot->setRobotId($this->id);
        $robot->setUrl($response->getUrl());
        $headers = '';

        foreach ($response->getHeaders() as $title => $value) {
            $headers .= sprintf("%s: %s\n", $title, $value[0]);
        }
        $robot->setHeaders($headers);
        var_dump($headers);
        $robot->setStatusCode($response->getStatusCode());
        $robot->setTimestamp(new \DateTime("NOW"));
        $robot->setContentType($response->getHeaders()['content-type'][0]);
        $robot->setData($response->getContent());
        $this->dataRepository->save($robot, true);
    }

    protected function findAll(string $url): void
    {
        $response = (new UrlScanner(HttpClient::create(), $url))->download();
        $this->store($response);
        $extractor = new UrlExtractor($response);
        $links = $extractor->extract('a');
        $filtered = (new UrlFilter($this->scheme, $this->host, $links))->filter();
        foreach ($filtered as $url) {
            if (!in_array($url, $this->visited)) {
                $this->visited[] = $url;
                $this->findAll($url);
            }
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        $settings = $this->repository->findAll()[0];

        $this->id = $settings->getId();
        $this->scheme = $settings->getScheme();
        $this->host = $settings->getDomainName();
        $this->url = sprintf("%s://%s", $this->scheme, $this->host);

        $this->findAll($this->url);

        $output->writeln(sprintf('id: %s', $id));

        return Command::SUCCESS;
    }
}
