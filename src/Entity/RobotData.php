<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RobotDataRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: RobotDataRepository::class)]
class RobotData
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(type: 'ulid')]
    private Ulid $robotId;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $timeStamp;

    #[ORM\Column]
    private int $statusCode;

    #[ORM\Column(type: Types::TEXT)]
    private string $headers;

    #[ORM\Column(type: Types::TEXT)]
    private string $url;

    #[ORM\Column(length: 255)]
    private string $contentType;

    #[ORM\Column(type: Types::BLOB)]
    private $data = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getRobotId(): Ulid
    {
        return $this->robotId;
    }

    public function setRobotId(Ulid $robotId): static
    {
        $this->robotId = $robotId;

        return $this;
    }

    public function getTimeStamp(): \DateTimeInterface
    {
        return $this->timeStamp;
    }

    public function setTimeStamp(\DateTimeInterface $timeStamp): static
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): static
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getHeaders(): string
    {
        return $this->headers;
    }

    public function setHeaders(string $headers): static
    {
        $this->headers = $headers;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): static
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data): static
    {
        $this->data = $data;

        return $this;
    }
}
