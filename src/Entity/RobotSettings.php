<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RobotSettingsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UlidType;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: RobotSettingsRepository::class)]
class RobotSettings
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\Column(type: UlidType::NAME, unique: true)]
    #[ORM\CustomIdGenerator(class: 'doctrine.ulid_generator')]
    private Ulid $id;

    #[ORM\Column(length: 255)]
    private string $domainName;

    #[ORM\Column(length: 16)]
    private string $scheme;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $startTime;

    #[ORM\Column]
    private int $retryMax;

    #[ORM\Column]
    private int $scanDelay;

    public function getId(): Ulid
    {
        return $this->id;
    }

    public function getDomainName(): string
    {
        return $this->domainName;
    }

    public function setDomainName(string $domainName): static
    {
        $this->domainName = $domainName;

        return $this;
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function setScheme(string $scheme): static
    {
        $this->scheme = $scheme;

        return $this;
    }

    public function getStartTime(): \DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): static
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getRetryMax(): int
    {
        return $this->retryMax;
    }

    public function setRetryMax(int $retryMax): static
    {
        $this->retryMax = $retryMax;

        return $this;
    }

    public function getScanDelay(): int
    {
        return $this->scanDelay;
    }

    public function setScanDelay(int $scanDelay): static
    {
        $this->scanDelay = $scanDelay;

        return $this;
    }
}
