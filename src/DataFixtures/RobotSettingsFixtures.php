<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\RobotSettings;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RobotSettingsFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $settings = new RobotSettings();

        $settings->setScheme('https');
        $settings->setDomainName('int6.co.uk');
        $settings->setStartTime(new \DateTime('13:00'));
        $settings->setRetryMax(5);
        $settings->setScanDelay(1);

        $manager->persist($settings);

        $manager->flush();
    }
}
